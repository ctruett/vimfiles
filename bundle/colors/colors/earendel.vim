" This scheme was created by CSApproxSnapshot
" on Fri, 14 Jun 2013

hi clear
if exists("syntax_on")
    syntax reset
endif

if v:version < 700
    let g:colors_name = expand("<sfile>:t:r")
    command! -nargs=+ CSAHi exe "hi" substitute(substitute(<q-args>, "undercurl", "underline", "g"), "guisp\\S\\+", "", "g")
else
    let g:colors_name = expand("<sfile>:t:r")
    command! -nargs=+ CSAHi exe "hi" <q-args>
endif

function! s:old_kde()
  " Konsole only used its own palette up til KDE 4.2.0
  if executable('kde4-config') && system('kde4-config --kde-version') =~ '^4.[10].'
    return 1
  elseif executable('kde-config') && system('kde-config --version') =~# 'KDE: 3.'
    return 1
  else
    return 0
  endif
endfunction

if 0
elseif has("gui_running") || (&t_Co == 256 && (&term ==# "xterm" || &term =~# "^screen") && exists("g:CSApprox_konsole") && g:CSApprox_konsole) || (&term =~? "^konsole" && s:old_kde())
    CSAHi Normal term=NONE cterm=NONE ctermbg=231 ctermfg=16 gui=NONE guibg=#f7f7f7 guifg=#000000
    CSAHi lCursor term=NONE cterm=NONE ctermbg=106 ctermfg=231 gui=NONE guibg=#79bf21 guifg=#ffffff
    CSAHi PreProc term=underline cterm=NONE ctermbg=231 ctermfg=131 gui=NONE guibg=bg guifg=#a33243
    CSAHi Special term=bold cterm=NONE ctermbg=231 ctermfg=95 gui=NONE guibg=bg guifg=#844631
    CSAHi Statement term=bold cterm=bold ctermbg=231 ctermfg=25 gui=bold guibg=bg guifg=#2239a8
    CSAHi Todo term=NONE cterm=bold ctermbg=221 ctermfg=52 gui=bold guibg=#fedc56 guifg=#512b1e
    CSAHi Type term=underline cterm=bold ctermbg=231 ctermfg=24 gui=bold guibg=bg guifg=#1d318d
    CSAHi Underlined term=underline cterm=underline ctermbg=231 ctermfg=19 gui=underline guibg=bg guifg=#272fc2
    CSAHi htmlBold term=NONE cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi htmlBoldItalic term=NONE cterm=bold ctermbg=231 ctermfg=16 gui=bold,italic guibg=bg guifg=fg
    CSAHi htmlBoldUnderline term=NONE cterm=bold,underline ctermbg=231 ctermfg=16 gui=bold,underline guibg=bg guifg=fg
    CSAHi helpNormal term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=231 ctermfg=95 gui=NONE guibg=bg guifg=#844631
    CSAHi NonText term=bold cterm=bold ctermbg=231 ctermfg=241 gui=bold guibg=bg guifg=#656565
    CSAHi Directory term=bold cterm=NONE ctermbg=231 ctermfg=19 gui=NONE guibg=bg guifg=#272fc2
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=160 ctermfg=231 gui=bold guibg=#ca001f guifg=#ffffff
    CSAHi IncSearch term=reverse cterm=NONE ctermbg=217 ctermfg=fg gui=NONE guibg=#f7b69d guifg=fg
    CSAHi Search term=reverse cterm=NONE ctermbg=222 ctermfg=fg gui=NONE guibg=#fee481 guifg=fg
    CSAHi MoreMsg term=bold cterm=bold ctermbg=231 ctermfg=239 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi ModeMsg term=bold cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi LineNr term=underline cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi EasyMotionTargetDefault term=NONE cterm=bold ctermbg=bg ctermfg=196 gui=bold guibg=bg guifg=#ff0000
    CSAHi cssStyle term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpGraphic term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptCommentSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi EasyMotionShadeDefault term=NONE cterm=NONE ctermbg=bg ctermfg=243 gui=NONE guibg=bg guifg=#777777
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#d16c7a
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#0f8674
    CSAHi Pmenu term=NONE cterm=NONE ctermbg=146 ctermfg=16 gui=NONE guibg=#aab8d5 guifg=fg
    CSAHi PmenuSel term=NONE cterm=NONE ctermbg=221 ctermfg=16 gui=NONE guibg=#fee06b guifg=fg
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=67 ctermfg=16 gui=NONE guibg=#6a83b5 guifg=fg
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=188 ctermfg=16 gui=NONE guibg=#c7cfe2 guifg=fg
    CSAHi TabLine term=underline cterm=underline ctermbg=188 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=underline ctermbg=188 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=254 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi cssLength term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptParens term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptValue term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlBoldUnderlineItalic term=NONE cterm=bold,underline ctermbg=231 ctermfg=16 gui=bold,italic,underline guibg=bg guifg=fg
    CSAHi htmlItalic term=NONE cterm=NONE ctermbg=231 ctermfg=16 gui=italic guibg=bg guifg=fg
    CSAHi htmlUnderline term=NONE cterm=underline ctermbg=231 ctermfg=16 gui=underline guibg=bg guifg=fg
    CSAHi htmlUnderlineItalic term=NONE cterm=underline ctermbg=231 ctermfg=16 gui=italic,underline guibg=bg guifg=fg
    CSAHi CursorLineNr term=bold cterm=bold ctermbg=bg ctermfg=124 gui=bold guibg=bg guifg=Brown
    CSAHi Question term=NONE cterm=bold ctermbg=231 ctermfg=239 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi StatusLine term=bold,reverse cterm=bold ctermbg=110 ctermfg=16 gui=bold guibg=#96aad3 guifg=fg
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=152 ctermfg=59 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=152 ctermfg=59 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi Title term=bold cterm=bold ctermbg=bg ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi Visual term=reverse cterm=NONE ctermbg=250 ctermfg=fg gui=NONE guibg=#bfbfbf guifg=fg
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=231 ctermfg=59 gui=bold,underline guibg=bg guifg=#324263
    CSAHi WarningMsg term=NONE cterm=bold ctermbg=231 ctermfg=160 gui=bold guibg=bg guifg=#ca001f
    CSAHi WildMenu term=NONE cterm=bold ctermbg=221 ctermfg=16 gui=bold guibg=#fedc56 guifg=fg
    CSAHi cssSpecialCharQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssMediaBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssDefinition term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesOdd term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesEven term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssPseudoClass term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssSpecialCharQQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlTagN term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssFontDescriptorBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorLine term=underline cterm=NONE ctermbg=254 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=188 ctermfg=fg gui=NONE guibg=#d5d5d5 guifg=fg
    CSAHi MatchParen term=reverse cterm=NONE ctermbg=30 ctermfg=231 gui=NONE guibg=#0f8674 guifg=#ffffff
    CSAHi Cursor term=NONE cterm=NONE ctermbg=16 ctermfg=231 gui=NONE guibg=#000000 guifg=#ffffff
    CSAHi Comment term=bold cterm=NONE ctermbg=231 ctermfg=64 gui=NONE guibg=bg guifg=#558817
    CSAHi Constant term=underline cterm=NONE ctermbg=231 ctermfg=130 gui=NONE guibg=bg guifg=#a8660d
    CSAHi Error term=reverse cterm=NONE ctermbg=231 ctermfg=124 gui=NONE guibg=bg guifg=#bf001d
    CSAHi Identifier term=underline cterm=NONE ctermbg=231 ctermfg=29 gui=NONE guibg=bg guifg=#0e7c6b
    CSAHi Ignore term=NONE cterm=NONE ctermbg=231 ctermfg=231 gui=NONE guibg=bg guifg=bg
    CSAHi cssString term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlight term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlightSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi None term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpLeadBlank term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi GReplaceText term=reverse cterm=reverse ctermbg=bg ctermfg=fg gui=reverse guibg=bg guifg=fg
    CSAHi cssAttributeSelector term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Folded term=NONE cterm=bold ctermbg=251 ctermfg=59 gui=bold guibg=#cacaca guifg=#324263
    CSAHi FoldColumn term=NONE cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=150 ctermfg=16 gui=NONE guibg=#bae981 guifg=fg
    CSAHi DiffChange term=bold cterm=NONE ctermbg=104 ctermfg=16 gui=NONE guibg=#8495e6 guifg=fg
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=211 ctermfg=16 gui=NONE guibg=#ff95a5 guifg=fg
    CSAHi DiffText term=reverse cterm=bold ctermbg=147 ctermfg=16 gui=bold guibg=#b9c2f0 guifg=fg
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi Conceal term=NONE cterm=NONE ctermbg=248 ctermfg=252 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#ca001f
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#272fc2
elseif has("gui_running") || (&t_Co == 256 && (&term ==# "xterm" || &term =~# "^screen") && exists("g:CSApprox_eterm") && g:CSApprox_eterm) || &term =~? "^eterm"
    CSAHi Normal term=NONE cterm=NONE ctermbg=231 ctermfg=16 gui=NONE guibg=#f7f7f7 guifg=#000000
    CSAHi lCursor term=NONE cterm=NONE ctermbg=106 ctermfg=231 gui=NONE guibg=#79bf21 guifg=#ffffff
    CSAHi PreProc term=underline cterm=NONE ctermbg=231 ctermfg=131 gui=NONE guibg=bg guifg=#a33243
    CSAHi Special term=bold cterm=NONE ctermbg=231 ctermfg=95 gui=NONE guibg=bg guifg=#844631
    CSAHi Statement term=bold cterm=bold ctermbg=231 ctermfg=25 gui=bold guibg=bg guifg=#2239a8
    CSAHi Todo term=NONE cterm=bold ctermbg=221 ctermfg=52 gui=bold guibg=#fedc56 guifg=#512b1e
    CSAHi Type term=underline cterm=bold ctermbg=231 ctermfg=24 gui=bold guibg=bg guifg=#1d318d
    CSAHi Underlined term=underline cterm=underline ctermbg=231 ctermfg=19 gui=underline guibg=bg guifg=#272fc2
    CSAHi htmlBold term=NONE cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi htmlBoldItalic term=NONE cterm=bold ctermbg=231 ctermfg=16 gui=bold,italic guibg=bg guifg=fg
    CSAHi htmlBoldUnderline term=NONE cterm=bold,underline ctermbg=231 ctermfg=16 gui=bold,underline guibg=bg guifg=fg
    CSAHi helpNormal term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=231 ctermfg=95 gui=NONE guibg=bg guifg=#844631
    CSAHi NonText term=bold cterm=bold ctermbg=231 ctermfg=241 gui=bold guibg=bg guifg=#656565
    CSAHi Directory term=bold cterm=NONE ctermbg=231 ctermfg=19 gui=NONE guibg=bg guifg=#272fc2
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=160 ctermfg=231 gui=bold guibg=#ca001f guifg=#ffffff
    CSAHi IncSearch term=reverse cterm=NONE ctermbg=217 ctermfg=fg gui=NONE guibg=#f7b69d guifg=fg
    CSAHi Search term=reverse cterm=NONE ctermbg=222 ctermfg=fg gui=NONE guibg=#fee481 guifg=fg
    CSAHi MoreMsg term=bold cterm=bold ctermbg=231 ctermfg=239 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi ModeMsg term=bold cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi LineNr term=underline cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi EasyMotionTargetDefault term=NONE cterm=bold ctermbg=bg ctermfg=196 gui=bold guibg=bg guifg=#ff0000
    CSAHi cssStyle term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpGraphic term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptCommentSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi EasyMotionShadeDefault term=NONE cterm=NONE ctermbg=bg ctermfg=243 gui=NONE guibg=bg guifg=#777777
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#d16c7a
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#0f8674
    CSAHi Pmenu term=NONE cterm=NONE ctermbg=146 ctermfg=16 gui=NONE guibg=#aab8d5 guifg=fg
    CSAHi PmenuSel term=NONE cterm=NONE ctermbg=221 ctermfg=16 gui=NONE guibg=#fee06b guifg=fg
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=67 ctermfg=16 gui=NONE guibg=#6a83b5 guifg=fg
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=188 ctermfg=16 gui=NONE guibg=#c7cfe2 guifg=fg
    CSAHi TabLine term=underline cterm=underline ctermbg=188 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=underline ctermbg=188 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=254 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi cssLength term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptParens term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptValue term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlBoldUnderlineItalic term=NONE cterm=bold,underline ctermbg=231 ctermfg=16 gui=bold,italic,underline guibg=bg guifg=fg
    CSAHi htmlItalic term=NONE cterm=NONE ctermbg=231 ctermfg=16 gui=italic guibg=bg guifg=fg
    CSAHi htmlUnderline term=NONE cterm=underline ctermbg=231 ctermfg=16 gui=underline guibg=bg guifg=fg
    CSAHi htmlUnderlineItalic term=NONE cterm=underline ctermbg=231 ctermfg=16 gui=italic,underline guibg=bg guifg=fg
    CSAHi CursorLineNr term=bold cterm=bold ctermbg=bg ctermfg=124 gui=bold guibg=bg guifg=Brown
    CSAHi Question term=NONE cterm=bold ctermbg=231 ctermfg=239 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi StatusLine term=bold,reverse cterm=bold ctermbg=110 ctermfg=16 gui=bold guibg=#96aad3 guifg=fg
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=152 ctermfg=59 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=152 ctermfg=59 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi Title term=bold cterm=bold ctermbg=bg ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi Visual term=reverse cterm=NONE ctermbg=250 ctermfg=fg gui=NONE guibg=#bfbfbf guifg=fg
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=231 ctermfg=59 gui=bold,underline guibg=bg guifg=#324263
    CSAHi WarningMsg term=NONE cterm=bold ctermbg=231 ctermfg=160 gui=bold guibg=bg guifg=#ca001f
    CSAHi WildMenu term=NONE cterm=bold ctermbg=221 ctermfg=16 gui=bold guibg=#fedc56 guifg=fg
    CSAHi cssSpecialCharQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssMediaBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssDefinition term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesOdd term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesEven term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssPseudoClass term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssSpecialCharQQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlTagN term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssFontDescriptorBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorLine term=underline cterm=NONE ctermbg=254 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=188 ctermfg=fg gui=NONE guibg=#d5d5d5 guifg=fg
    CSAHi MatchParen term=reverse cterm=NONE ctermbg=30 ctermfg=231 gui=NONE guibg=#0f8674 guifg=#ffffff
    CSAHi Cursor term=NONE cterm=NONE ctermbg=16 ctermfg=231 gui=NONE guibg=#000000 guifg=#ffffff
    CSAHi Comment term=bold cterm=NONE ctermbg=231 ctermfg=64 gui=NONE guibg=bg guifg=#558817
    CSAHi Constant term=underline cterm=NONE ctermbg=231 ctermfg=130 gui=NONE guibg=bg guifg=#a8660d
    CSAHi Error term=reverse cterm=NONE ctermbg=231 ctermfg=124 gui=NONE guibg=bg guifg=#bf001d
    CSAHi Identifier term=underline cterm=NONE ctermbg=231 ctermfg=29 gui=NONE guibg=bg guifg=#0e7c6b
    CSAHi Ignore term=NONE cterm=NONE ctermbg=231 ctermfg=231 gui=NONE guibg=bg guifg=bg
    CSAHi cssString term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlight term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlightSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi None term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpLeadBlank term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi GReplaceText term=reverse cterm=reverse ctermbg=bg ctermfg=fg gui=reverse guibg=bg guifg=fg
    CSAHi cssAttributeSelector term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Folded term=NONE cterm=bold ctermbg=251 ctermfg=59 gui=bold guibg=#cacaca guifg=#324263
    CSAHi FoldColumn term=NONE cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=150 ctermfg=16 gui=NONE guibg=#bae981 guifg=fg
    CSAHi DiffChange term=bold cterm=NONE ctermbg=104 ctermfg=16 gui=NONE guibg=#8495e6 guifg=fg
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=211 ctermfg=16 gui=NONE guibg=#ff95a5 guifg=fg
    CSAHi DiffText term=reverse cterm=bold ctermbg=147 ctermfg=16 gui=bold guibg=#b9c2f0 guifg=fg
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi Conceal term=NONE cterm=NONE ctermbg=248 ctermfg=252 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#ca001f
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#272fc2
elseif has("gui_running") || &t_Co == 256
    CSAHi Normal term=NONE cterm=NONE ctermbg=231 ctermfg=16 gui=NONE guibg=#f7f7f7 guifg=#000000
    CSAHi lCursor term=NONE cterm=NONE ctermbg=106 ctermfg=231 gui=NONE guibg=#79bf21 guifg=#ffffff
    CSAHi PreProc term=underline cterm=NONE ctermbg=231 ctermfg=131 gui=NONE guibg=bg guifg=#a33243
    CSAHi Special term=bold cterm=NONE ctermbg=231 ctermfg=95 gui=NONE guibg=bg guifg=#844631
    CSAHi Statement term=bold cterm=bold ctermbg=231 ctermfg=25 gui=bold guibg=bg guifg=#2239a8
    CSAHi Todo term=NONE cterm=bold ctermbg=221 ctermfg=52 gui=bold guibg=#fedc56 guifg=#512b1e
    CSAHi Type term=underline cterm=bold ctermbg=231 ctermfg=24 gui=bold guibg=bg guifg=#1d318d
    CSAHi Underlined term=underline cterm=underline ctermbg=231 ctermfg=19 gui=underline guibg=bg guifg=#272fc2
    CSAHi htmlBold term=NONE cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi htmlBoldItalic term=NONE cterm=bold ctermbg=231 ctermfg=16 gui=bold,italic guibg=bg guifg=fg
    CSAHi htmlBoldUnderline term=NONE cterm=bold,underline ctermbg=231 ctermfg=16 gui=bold,underline guibg=bg guifg=fg
    CSAHi helpNormal term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=231 ctermfg=95 gui=NONE guibg=bg guifg=#844631
    CSAHi NonText term=bold cterm=bold ctermbg=231 ctermfg=241 gui=bold guibg=bg guifg=#656565
    CSAHi Directory term=bold cterm=NONE ctermbg=231 ctermfg=19 gui=NONE guibg=bg guifg=#272fc2
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=160 ctermfg=231 gui=bold guibg=#ca001f guifg=#ffffff
    CSAHi IncSearch term=reverse cterm=NONE ctermbg=217 ctermfg=fg gui=NONE guibg=#f7b69d guifg=fg
    CSAHi Search term=reverse cterm=NONE ctermbg=222 ctermfg=fg gui=NONE guibg=#fee481 guifg=fg
    CSAHi MoreMsg term=bold cterm=bold ctermbg=231 ctermfg=239 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi ModeMsg term=bold cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi LineNr term=underline cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi EasyMotionTargetDefault term=NONE cterm=bold ctermbg=bg ctermfg=196 gui=bold guibg=bg guifg=#ff0000
    CSAHi cssStyle term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpGraphic term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptCommentSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi EasyMotionShadeDefault term=NONE cterm=NONE ctermbg=bg ctermfg=243 gui=NONE guibg=bg guifg=#777777
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#d16c7a
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#0f8674
    CSAHi Pmenu term=NONE cterm=NONE ctermbg=146 ctermfg=16 gui=NONE guibg=#aab8d5 guifg=fg
    CSAHi PmenuSel term=NONE cterm=NONE ctermbg=221 ctermfg=16 gui=NONE guibg=#fee06b guifg=fg
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=67 ctermfg=16 gui=NONE guibg=#6a83b5 guifg=fg
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=188 ctermfg=16 gui=NONE guibg=#c7cfe2 guifg=fg
    CSAHi TabLine term=underline cterm=underline ctermbg=188 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=231 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=underline ctermbg=188 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=254 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi cssLength term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptParens term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptValue term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlBoldUnderlineItalic term=NONE cterm=bold,underline ctermbg=231 ctermfg=16 gui=bold,italic,underline guibg=bg guifg=fg
    CSAHi htmlItalic term=NONE cterm=NONE ctermbg=231 ctermfg=16 gui=italic guibg=bg guifg=fg
    CSAHi htmlUnderline term=NONE cterm=underline ctermbg=231 ctermfg=16 gui=underline guibg=bg guifg=fg
    CSAHi htmlUnderlineItalic term=NONE cterm=underline ctermbg=231 ctermfg=16 gui=italic,underline guibg=bg guifg=fg
    CSAHi CursorLineNr term=bold cterm=bold ctermbg=bg ctermfg=124 gui=bold guibg=bg guifg=Brown
    CSAHi Question term=NONE cterm=bold ctermbg=231 ctermfg=239 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi StatusLine term=bold,reverse cterm=bold ctermbg=110 ctermfg=16 gui=bold guibg=#96aad3 guifg=fg
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=152 ctermfg=59 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=152 ctermfg=59 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi Title term=bold cterm=bold ctermbg=bg ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi Visual term=reverse cterm=NONE ctermbg=250 ctermfg=fg gui=NONE guibg=#bfbfbf guifg=fg
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=231 ctermfg=59 gui=bold,underline guibg=bg guifg=#324263
    CSAHi WarningMsg term=NONE cterm=bold ctermbg=231 ctermfg=160 gui=bold guibg=bg guifg=#ca001f
    CSAHi WildMenu term=NONE cterm=bold ctermbg=221 ctermfg=16 gui=bold guibg=#fedc56 guifg=fg
    CSAHi cssSpecialCharQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssMediaBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssDefinition term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesOdd term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesEven term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssPseudoClass term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssSpecialCharQQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlTagN term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssFontDescriptorBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorLine term=underline cterm=NONE ctermbg=254 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=188 ctermfg=fg gui=NONE guibg=#d5d5d5 guifg=fg
    CSAHi MatchParen term=reverse cterm=NONE ctermbg=30 ctermfg=231 gui=NONE guibg=#0f8674 guifg=#ffffff
    CSAHi Cursor term=NONE cterm=NONE ctermbg=16 ctermfg=231 gui=NONE guibg=#000000 guifg=#ffffff
    CSAHi Comment term=bold cterm=NONE ctermbg=231 ctermfg=64 gui=NONE guibg=bg guifg=#558817
    CSAHi Constant term=underline cterm=NONE ctermbg=231 ctermfg=130 gui=NONE guibg=bg guifg=#a8660d
    CSAHi Error term=reverse cterm=NONE ctermbg=231 ctermfg=124 gui=NONE guibg=bg guifg=#bf001d
    CSAHi Identifier term=underline cterm=NONE ctermbg=231 ctermfg=29 gui=NONE guibg=bg guifg=#0e7c6b
    CSAHi Ignore term=NONE cterm=NONE ctermbg=231 ctermfg=231 gui=NONE guibg=bg guifg=bg
    CSAHi cssString term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlight term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlightSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi None term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpLeadBlank term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi GReplaceText term=reverse cterm=reverse ctermbg=bg ctermfg=fg gui=reverse guibg=bg guifg=fg
    CSAHi cssAttributeSelector term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Folded term=NONE cterm=bold ctermbg=251 ctermfg=59 gui=bold guibg=#cacaca guifg=#324263
    CSAHi FoldColumn term=NONE cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=150 ctermfg=16 gui=NONE guibg=#bae981 guifg=fg
    CSAHi DiffChange term=bold cterm=NONE ctermbg=104 ctermfg=16 gui=NONE guibg=#8495e6 guifg=fg
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=211 ctermfg=16 gui=NONE guibg=#ff95a5 guifg=fg
    CSAHi DiffText term=reverse cterm=bold ctermbg=147 ctermfg=16 gui=bold guibg=#b9c2f0 guifg=fg
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=231 ctermfg=241 gui=NONE guibg=bg guifg=#656565
    CSAHi Conceal term=NONE cterm=NONE ctermbg=248 ctermfg=252 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#ca001f
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#272fc2
elseif has("gui_running") || &t_Co == 88
    CSAHi Normal term=NONE cterm=NONE ctermbg=79 ctermfg=16 gui=NONE guibg=#f7f7f7 guifg=#000000
    CSAHi lCursor term=NONE cterm=NONE ctermbg=40 ctermfg=79 gui=NONE guibg=#79bf21 guifg=#ffffff
    CSAHi PreProc term=underline cterm=NONE ctermbg=79 ctermfg=32 gui=NONE guibg=bg guifg=#a33243
    CSAHi Special term=bold cterm=NONE ctermbg=79 ctermfg=36 gui=NONE guibg=bg guifg=#844631
    CSAHi Statement term=bold cterm=bold ctermbg=79 ctermfg=17 gui=bold guibg=bg guifg=#2239a8
    CSAHi Todo term=NONE cterm=bold ctermbg=73 ctermfg=32 gui=bold guibg=#fedc56 guifg=#512b1e
    CSAHi Type term=underline cterm=bold ctermbg=79 ctermfg=17 gui=bold guibg=bg guifg=#1d318d
    CSAHi Underlined term=underline cterm=underline ctermbg=79 ctermfg=18 gui=underline guibg=bg guifg=#272fc2
    CSAHi htmlBold term=NONE cterm=bold ctermbg=79 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi htmlBoldItalic term=NONE cterm=bold ctermbg=79 ctermfg=16 gui=bold,italic guibg=bg guifg=fg
    CSAHi htmlBoldUnderline term=NONE cterm=bold,underline ctermbg=79 ctermfg=16 gui=bold,underline guibg=bg guifg=fg
    CSAHi helpNormal term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi SpecialKey term=bold cterm=NONE ctermbg=79 ctermfg=36 gui=NONE guibg=bg guifg=#844631
    CSAHi NonText term=bold cterm=bold ctermbg=79 ctermfg=81 gui=bold guibg=bg guifg=#656565
    CSAHi Directory term=bold cterm=NONE ctermbg=79 ctermfg=18 gui=NONE guibg=bg guifg=#272fc2
    CSAHi ErrorMsg term=NONE cterm=bold ctermbg=48 ctermfg=79 gui=bold guibg=#ca001f guifg=#ffffff
    CSAHi IncSearch term=reverse cterm=NONE ctermbg=73 ctermfg=fg gui=NONE guibg=#f7b69d guifg=fg
    CSAHi Search term=reverse cterm=NONE ctermbg=73 ctermfg=fg gui=NONE guibg=#fee481 guifg=fg
    CSAHi MoreMsg term=bold cterm=bold ctermbg=79 ctermfg=81 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi ModeMsg term=bold cterm=bold ctermbg=79 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi LineNr term=underline cterm=NONE ctermbg=79 ctermfg=81 gui=NONE guibg=bg guifg=#656565
    CSAHi EasyMotionTargetDefault term=NONE cterm=bold ctermbg=bg ctermfg=64 gui=bold guibg=bg guifg=#ff0000
    CSAHi cssStyle term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpGraphic term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptCommentSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi EasyMotionShadeDefault term=NONE cterm=NONE ctermbg=bg ctermfg=82 gui=NONE guibg=bg guifg=#777777
    CSAHi SpellRare term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#d16c7a
    CSAHi SpellLocal term=underline cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#0f8674
    CSAHi Pmenu term=NONE cterm=NONE ctermbg=42 ctermfg=16 gui=NONE guibg=#aab8d5 guifg=fg
    CSAHi PmenuSel term=NONE cterm=NONE ctermbg=73 ctermfg=16 gui=NONE guibg=#fee06b guifg=fg
    CSAHi PmenuSbar term=NONE cterm=NONE ctermbg=38 ctermfg=16 gui=NONE guibg=#6a83b5 guifg=fg
    CSAHi PmenuThumb term=NONE cterm=NONE ctermbg=58 ctermfg=16 gui=NONE guibg=#c7cfe2 guifg=fg
    CSAHi TabLine term=underline cterm=underline ctermbg=86 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi TabLineSel term=bold cterm=bold ctermbg=79 ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi TabLineFill term=reverse cterm=underline ctermbg=86 ctermfg=16 gui=underline guibg=#d4d4d4 guifg=fg
    CSAHi CursorColumn term=reverse cterm=NONE ctermbg=87 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi cssLength term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptParens term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi javaScriptValue term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlBoldUnderlineItalic term=NONE cterm=bold,underline ctermbg=79 ctermfg=16 gui=bold,italic,underline guibg=bg guifg=fg
    CSAHi htmlItalic term=NONE cterm=NONE ctermbg=79 ctermfg=16 gui=italic guibg=bg guifg=fg
    CSAHi htmlUnderline term=NONE cterm=underline ctermbg=79 ctermfg=16 gui=underline guibg=bg guifg=fg
    CSAHi htmlUnderlineItalic term=NONE cterm=underline ctermbg=79 ctermfg=16 gui=italic,underline guibg=bg guifg=fg
    CSAHi CursorLineNr term=bold cterm=bold ctermbg=bg ctermfg=32 gui=bold guibg=bg guifg=Brown
    CSAHi Question term=NONE cterm=bold ctermbg=79 ctermfg=81 gui=bold guibg=bg guifg=#4a4a4a
    CSAHi StatusLine term=bold,reverse cterm=bold ctermbg=38 ctermfg=16 gui=bold guibg=#96aad3 guifg=fg
    CSAHi StatusLineNC term=reverse cterm=NONE ctermbg=58 ctermfg=17 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi VertSplit term=reverse cterm=NONE ctermbg=58 ctermfg=17 gui=NONE guibg=#bcc7de guifg=#384547
    CSAHi Title term=bold cterm=bold ctermbg=bg ctermfg=16 gui=bold guibg=bg guifg=fg
    CSAHi Visual term=reverse cterm=NONE ctermbg=85 ctermfg=fg gui=NONE guibg=#bfbfbf guifg=fg
    CSAHi VisualNOS term=bold,underline cterm=bold,underline ctermbg=79 ctermfg=17 gui=bold,underline guibg=bg guifg=#324263
    CSAHi WarningMsg term=NONE cterm=bold ctermbg=79 ctermfg=48 gui=bold guibg=bg guifg=#ca001f
    CSAHi WildMenu term=NONE cterm=bold ctermbg=73 ctermfg=16 gui=bold guibg=#fedc56 guifg=fg
    CSAHi cssSpecialCharQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssMediaBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssDefinition term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesOdd term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi IndentGuidesEven term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssPseudoClass term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssSpecialCharQQ term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlTagN term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi cssFontDescriptorBlock term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi CursorLine term=underline cterm=NONE ctermbg=87 ctermfg=fg gui=NONE guibg=#e0e0e0 guifg=fg
    CSAHi ColorColumn term=reverse cterm=NONE ctermbg=86 ctermfg=fg gui=NONE guibg=#d5d5d5 guifg=fg
    CSAHi MatchParen term=reverse cterm=NONE ctermbg=21 ctermfg=79 gui=NONE guibg=#0f8674 guifg=#ffffff
    CSAHi Cursor term=NONE cterm=NONE ctermbg=16 ctermfg=79 gui=NONE guibg=#000000 guifg=#ffffff
    CSAHi Comment term=bold cterm=NONE ctermbg=79 ctermfg=36 gui=NONE guibg=bg guifg=#558817
    CSAHi Constant term=underline cterm=NONE ctermbg=79 ctermfg=36 gui=NONE guibg=bg guifg=#a8660d
    CSAHi Error term=reverse cterm=NONE ctermbg=79 ctermfg=48 gui=NONE guibg=bg guifg=#bf001d
    CSAHi Identifier term=underline cterm=NONE ctermbg=79 ctermfg=21 gui=NONE guibg=bg guifg=#0e7c6b
    CSAHi Ignore term=NONE cterm=NONE ctermbg=79 ctermfg=79 gui=NONE guibg=bg guifg=bg
    CSAHi cssString term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlight term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi htmlHighlightSkip term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi None term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi helpLeadBlank term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi GReplaceText term=reverse cterm=reverse ctermbg=bg ctermfg=fg gui=reverse guibg=bg guifg=fg
    CSAHi cssAttributeSelector term=NONE cterm=NONE ctermbg=bg ctermfg=fg gui=NONE guibg=bg guifg=fg
    CSAHi Folded term=NONE cterm=bold ctermbg=58 ctermfg=17 gui=bold guibg=#cacaca guifg=#324263
    CSAHi FoldColumn term=NONE cterm=NONE ctermbg=79 ctermfg=81 gui=NONE guibg=bg guifg=#656565
    CSAHi DiffAdd term=bold cterm=NONE ctermbg=61 ctermfg=16 gui=NONE guibg=#bae981 guifg=fg
    CSAHi DiffChange term=bold cterm=NONE ctermbg=38 ctermfg=16 gui=NONE guibg=#8495e6 guifg=fg
    CSAHi DiffDelete term=bold cterm=NONE ctermbg=69 ctermfg=16 gui=NONE guibg=#ff95a5 guifg=fg
    CSAHi DiffText term=reverse cterm=bold ctermbg=59 ctermfg=16 gui=bold guibg=#b9c2f0 guifg=fg
    CSAHi SignColumn term=NONE cterm=NONE ctermbg=79 ctermfg=81 gui=NONE guibg=bg guifg=#656565
    CSAHi Conceal term=NONE cterm=NONE ctermbg=84 ctermfg=86 gui=NONE guibg=DarkGrey guifg=LightGrey
    CSAHi SpellBad term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#ca001f
    CSAHi SpellCap term=reverse cterm=undercurl ctermbg=bg ctermfg=fg gui=undercurl guibg=bg guifg=fg guisp=#272fc2
endif

if 1
    delcommand CSAHi
endif
